package ru.demo.listdir;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Arrays;

/**
 * Класс демонстрирует вывод списка содержимого указанного каталога
 * и отбор файлов с расширением .mp3
 *
 * @author ITA
 */
public class ListDir {

    private static final String PATH_NAME = "D:\\work\\musicMP3";

    public static void main(String[] args) {
        File pathDirectory = new File(PATH_NAME);
        String[] listDir = pathDirectory.list();

        System.out.println(Arrays.toString(listDir));

        if (listDir == null) {
            try {
                throw new Exception("Ошибка ввода-вывода");
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        System.out.println("Первый способ отбора файлов\n");
        for (String fileName : listDir) {
            if (fileName.matches(".+\\.mp3")) {
                System.out.println(fileName);
            }
        }

        System.out.println("\nВторой способ отбора файлов\n");
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(PATH_NAME), "*.mp3")) {
            for (Path entry : directoryStream) {
                System.out.println(entry.getFileName());
            }
        } catch (InvalidPathException e) {
            System.out.println("OшиOкa указания пути " + e);
        } catch (NotDirectoryException e) {
            System.out.println(PATH_NAME + "не является каталогом.");
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода: " + e);
        }
    }
}

